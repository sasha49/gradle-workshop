package nl.craftsmen.gradle.workshop.docker.tasks


import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

abstract class StartContainerTask extends DefaultTask {

    public static final Long DEFAULT_START_DELAY = 5000L

    @Input
    abstract Property<String> getImage()

    @Input
    abstract Property<Integer> getPort()

    @Input
    abstract Property<Long> getStartDelay()

    StartContainerTask() {
        startDelay.convention(DEFAULT_START_DELAY)
    }

    @TaskAction
    def startContainer() {
        nl.craftsmen.gradle.workshop.docker.StaticDockerContainer.start(image.get(), port.get())
        Thread.sleep(startDelay.get())
    }

}

