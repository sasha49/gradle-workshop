package nl.craftsmen.gradle.workshop.docker.tasks


import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

abstract class StopContainerTask extends DefaultTask {
    @TaskAction
    void stop() {
        nl.craftsmen.gradle.workshop.docker.StaticDockerContainer.stop()
    }
}
