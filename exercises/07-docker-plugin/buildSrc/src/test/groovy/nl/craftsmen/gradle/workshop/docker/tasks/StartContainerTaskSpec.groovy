package nl.craftsmen.gradle.workshop.docker.tasks

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class StartContainerTaskSpec extends Specification {


    def "can add task to project"() {
        setup:
        Project project = ProjectBuilder.builder().build()

        when:
        def task = project.task('startContainerTestTask', type: StartContainerTask)

        then:
        task instanceof StartContainerTask
    }

}
