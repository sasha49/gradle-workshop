package nl.craftsmen.gradle.workshop.docker.plugin

abstract class GitExtension {
    abstract RegularFileProperty getCloneDirectory()

    abstract List<Property<String>> getRepositories()

    def GitExtension() {
        cloneDirectory.convention('build/git')
    }
}
