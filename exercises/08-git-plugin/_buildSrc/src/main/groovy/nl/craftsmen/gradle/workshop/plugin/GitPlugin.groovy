package nl.craftsmen.gradle.workshop.docker.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

class GitPlugin extends Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create('git', GitExtension)
        project.tasks.register("gitUpdate", GitPullTask) {

        }
        project.tasks.register("gitUpdateExample1", GitUpdateTask) {

        }
        project.tasks.register("clean", CleanTask) {

        }
    }

}
