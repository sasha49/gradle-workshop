package example.services

import spock.lang.Specification

class ServicesSpec extends Specification {

    def GREETING = "Hello"
    def NAME = "test"

    def "expect service to return name"() {
        when:
        def service = new Service()

        then:
        service.sayHello(NAME) == "$GREETING $NAME"
    }

}
